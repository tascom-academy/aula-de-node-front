import moment from 'moment'

const compareDate = (serverDate, date) => {
  let timeDiff = moment(serverDate).diff(moment(date+'z'), 'seconds')
  let timeDiffStr;
  if (timeDiff < 0) timeDiff = timeDiff * -1

  if (timeDiff < 60) timeDiffStr = `${timeDiff} second(s) ago...`;
  if (timeDiff >= 60) timeDiffStr = `${Math.floor(timeDiff / 60)} minute(s) ago...`;
  if (timeDiff >= 3600) timeDiffStr = `${Math.floor(timeDiff / 3600)} hour(s) ago...`;
  if (timeDiff >= 86400) timeDiffStr = `${Math.floor(timeDiff / 86400)} day(s) ago...`;
  if (timeDiff > 2419200) timeDiffStr = moment(date).toLocaleDateString();

  return timeDiffStr;
};

export default compareDate;
